﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Sitecore Corporation")]
[assembly: AssemblyCopyright("Copyright \x00a9 Sitecore Corporation 1999-2014")]
[assembly: AssemblyProduct("Sitecore Services 1.0.0")]
[assembly: AssemblyDescription("Sitecore Services")]
[assembly: AssemblyTrademark("Sitecore\x00ae is a registered trademark of Sitecore Corporation.")]

[assembly: ComVisible(false)]
[assembly: CLSCompliant(false)]

[assembly: NeutralResourcesLanguage("en-GB")]