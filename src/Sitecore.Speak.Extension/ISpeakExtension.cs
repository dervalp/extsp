﻿using Sitecore.Data;
using Sitecore.Data.Managers;
using Sitecore.Data.Templates;
using Sitecore.Mvc.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sitecore.Speak.Extension
{


  public interface ISpeakContext
  {

    void addModule(Data.ID moduleId, string placeHolderName);
    void getComponent(string Id);
  }

  public interface ISpeakExtension
  {
    void Extend(ISpeakContext speak);
  }
}
