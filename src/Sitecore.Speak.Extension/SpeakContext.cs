﻿using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Templates;
using Sitecore.Mvc.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Sitecore.Speak.Extension
{
  public class SpeakContext : ISpeakContext
  {
    private RenderingContext _renderingContext;
    private PageDefinition _currentPageDefinition;
    private string _moduleTemplateId = "xxxx"; //this should represent the ID of the SPEAK Item

    private bool _isDerived([NotNull] Template template, [NotNull] ID templateId)
    {
      return template.ID == templateId || template.GetBaseTemplates().Any(baseTemplate => _isDerived(baseTemplate, templateId));
    }

    public SpeakContext(RenderingContext context)
    {
      _renderingContext = context;
      _currentPageDefinition = context.PageContext.PageDefinition;
    }

    /// <summary>
    /// Return all renderings from an item defined on a device
    /// </summary>
    private Sitecore.Layouts.RenderingReference[] GetRenderingReferences(Sitecore.Data.Items.Item item, string deviceName)
    {
      Sitecore.Data.Fields.LayoutField layoutField = item.Fields["__renderings"];
      Sitecore.Layouts.RenderingReference[] renderings = layoutField.GetReferences(GetDeviceItem(item.Database, deviceName));
      return renderings;
    }

    /// <summary>
    /// Get the device item from a device name
    /// </summary>
    private Sitecore.Data.Items.DeviceItem GetDeviceItem(Sitecore.Data.Database db, string deviceName)
    {
      return db.Resources.Devices.GetAll().Where(d => d.Name.ToLower() == deviceName.ToLower()).First();
    }

    protected XElement GetPage()
    {
      Item obj = PageContext.Current.Item;
      if (obj == null)
        return (XElement)null;
      Field field = obj.Fields[FieldIDs.LayoutField];
      if (field == null)
        return (XElement)null;
      string fieldValue = LayoutField.GetFieldValue(field);
     
      return XDocument.Parse(fieldValue).Root;
    }

    protected XElement GetFromField(Item item)
    {
      Item obj = item;
      if (obj == null)
        return (XElement)null;
      Field field = obj.Fields[FieldIDs.LayoutField];
      if (field == null)
        return (XElement)null;

      string fieldValue = LayoutField.GetFieldValue(field);

      return XDocument.Parse(fieldValue).Root;
    }

    private IEnumerable<string> GetPlaceholderKeys(Sitecore.Data.Items.Item item)
    {
      List<string> uniquePlaceholderKeys = new List<string>();
      Sitecore.Layouts.RenderingReference[] renderings = GetRenderingReferences(item, "default");
      foreach (var rendering in renderings)
      {
        if (!uniquePlaceholderKeys.Contains(rendering.Placeholder))
          uniquePlaceholderKeys.Add(rendering.Placeholder);
      }


      return uniquePlaceholderKeys;
    }

    public void addModule(Data.ID moduleId, string placeHolderName)
    {
      var moduleItem = Sitecore.Context.Database.GetItem(moduleId);
      var xElement = GetFromField(moduleItem);
      var pageElement = GetPage();

      
      
      var placeHolderKeys = GetPlaceholderKeys(_renderingContext.PageContext.Item);

      //_currentPageDefinition.Renderings.Add(moduleItem);
      //Sitecore.Data.Items.DeviceItem device = Sitecore.Context.Device;

      //Sitecore.Layouts.RenderingReference[] currentRenderings = moduleItem.Visualization.GetRenderings(device, true);
      

    }

    public void getComponent(string Id)
    {

    }
  }
}
